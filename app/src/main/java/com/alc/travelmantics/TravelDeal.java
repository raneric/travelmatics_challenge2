package com.alc.travelmantics;

import java.io.Serializable;

public class TravelDeal implements Serializable {

    private String id;
    private String travelTitle;
    private String description;
    private String travelPrice;
    private String pictureName;

    public String getPictureName() {
        return pictureName;
    }

    public void setPictureName(String pictureName) {
        this.pictureName = pictureName;
    }

    public TravelDeal() {
    }

    public TravelDeal(String travelTitle, String description, String travelPrice, String imageUrl, String pictureName) {
        this.travelTitle = travelTitle;
        this.description = description;
        this.travelPrice = travelPrice;
        this.imageUrl = imageUrl;
        this.pictureName = pictureName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTravelTitle() {
        return travelTitle;
    }

    public void setTravelTitle(String travelTitle) {
        this.travelTitle = travelTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTravelPrice() {
        return travelPrice;
    }

    public void setTravelPrice(String travelPrice) {
        this.travelPrice = travelPrice;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    private String imageUrl;


}
