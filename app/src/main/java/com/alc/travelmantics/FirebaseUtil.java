package com.alc.travelmantics;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


public class FirebaseUtil {

    public static ArrayList<TravelDeal> sDeals;
    public static FirebaseDatabase sFirebaseDatabase;
    public static DatabaseReference sDatabaseReference;
    public static FirebaseUtil sFirebaseUtil;
    public static FirebaseAuth mFirebaseAuth;
    public static FirebaseStorage sStorage;
    public static StorageReference sStorageReference;
    public static FirebaseAuth.AuthStateListener mAuthStateListener;
    private static final int RC_SIGN_IN = 123;
    public static final String DATA_REFERENCE = "travelDeals";
    public static final String ADMIN_REFERENCE = "administrators";
    private static ListeActivity caller;
    public static boolean isAdmin;


    private FirebaseUtil() {

    }

    public static void openFbReference(String ref, final ListeActivity callerActivity) {
        if (sFirebaseUtil == null) {
            sFirebaseUtil = new FirebaseUtil();
            sFirebaseDatabase = FirebaseDatabase.getInstance();
            mFirebaseAuth = FirebaseAuth.getInstance();
            caller = callerActivity;
            mAuthStateListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    if (firebaseAuth.getCurrentUser() == null) {
                        FirebaseUtil.singIn();
                        Toast.makeText(callerActivity.getBaseContext(),
                                       "Welcome back to Travelmatics",
                                       Toast.LENGTH_LONG).show();
                    } else {
                        String userId = firebaseAuth.getUid();
                        checkIfAdmin(userId);
                    }
                }
            };
            connectStorage();
        }
        sDeals = new ArrayList<TravelDeal>();
        sDatabaseReference = sFirebaseDatabase.getReference().child(ref);

    }

    public static void checkIfAdmin(String userId) {



        DatabaseReference ref = sFirebaseDatabase.getReference().child(ADMIN_REFERENCE).child(userId);
        ChildEventListener listner = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                FirebaseUtil.isAdmin = true;
                caller.showMenu();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                FirebaseUtil.isAdmin = false;
            }
        };

        ref.addChildEventListener(listner);

    }

    public static void singIn() {
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build());

        caller.startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);

    }

    public static void atachlistener() {
        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }

    public static void detachlistener() {
        mFirebaseAuth.removeAuthStateListener(mAuthStateListener);
    }

    public static void connectStorage() {
        sStorage = FirebaseStorage.getInstance();
        sStorageReference = sStorage.getReference().child("deal_pictures");
    }
}
