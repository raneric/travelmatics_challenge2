package com.alc.travelmantics;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ListeActivity extends AppCompatActivity {

    ArrayList<TravelDeal> deals;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;
    private ChildEventListener mChildEventListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.liste_activity_menu, menu);
        MenuItem menuNewTravel = menu.findItem(R.id.menu_new_travel_deal);

        if (FirebaseUtil.isAdmin) {
            menuNewTravel.setVisible(true);
        } else {
            menuNewTravel.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_new_travel_deal:
                Intent intent = new Intent(this, DealActivity.class);
                startActivity(intent);
                return true;
            case R.id.menu_logout:
                AuthUI.getInstance()
                        .signOut(this)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            public void onComplete(@NonNull Task<Void> task) {
                                FirebaseUtil.atachlistener();
                            }
                        });
                FirebaseUtil.isAdmin = false;
                FirebaseUtil.detachlistener();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        FirebaseUtil.detachlistener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FirebaseUtil.openFbReference(FirebaseUtil.DATA_REFERENCE, this);
        invalidateOptionsMenu();
        RecyclerView dealList = (RecyclerView) findViewById(R.id.deal_liste);
        final DealAdapter adapter = new DealAdapter();
        dealList.setAdapter(adapter);
        LinearLayoutManager dealLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        dealList.setLayoutManager(dealLayoutManager);
        FirebaseUtil.atachlistener();
    }

    public void showMenu() {
        invalidateOptionsMenu();
    }

}
