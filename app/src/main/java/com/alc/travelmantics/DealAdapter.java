package com.alc.travelmantics;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

public class DealAdapter extends RecyclerView.Adapter<DealAdapter.DealViewHolder> {

    ArrayList<TravelDeal> deals;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;
    private ChildEventListener mChildEventListener;
    private ImageView mDealImage;

    /**
     * ******************************************************
     * ************ DealAdapter Constructor *****************
     * ******************************************************
     */
    public DealAdapter() {

        mFirebaseDatabase = FirebaseUtil.sFirebaseDatabase;
        mDatabaseReference = FirebaseUtil.sDatabaseReference;
        this.deals = FirebaseUtil.sDeals;
        mChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                TravelDeal travelDeal = dataSnapshot.getValue(TravelDeal.class);
                travelDeal.setId(dataSnapshot.getKey());
                deals.add(travelDeal);
                notifyItemInserted(deals.size() - 1);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        mDatabaseReference.addChildEventListener(mChildEventListener);


    }

    @NonNull
    @Override
    public DealViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.deal_liste_item, parent, false);

        return new DealViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DealViewHolder holder, int position) {
        TravelDeal travelDeal = deals.get(position);
        holder.bind(travelDeal);

    }

    @Override
    public int getItemCount() {
        return deals.size();
    }


    /**
     * **************************************************
     * ************ DealViewHolder nested class *********
     * **************************************************
     */
    public class DealViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textViewtitle;
        TextView textViewDescription;
        TextView textViewPrice;

        public DealViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewtitle = (TextView) itemView.findViewById(R.id.txt_view_travel_title);
            textViewDescription = (TextView) itemView.findViewById(R.id.txt_view_travel_description);
            textViewPrice = (TextView) itemView.findViewById(R.id.txt_view_price);
            mDealImage = (ImageView) itemView.findViewById(R.id.travel_image);
            itemView.setOnClickListener(this);
        }

        public void bind(TravelDeal deal) {
            textViewtitle.setText(deal.getTravelTitle());
            textViewDescription.setText(deal.getDescription());
            textViewPrice.setText(deal.getTravelPrice());
            showImage(deal.getImageUrl());
        }

        @Override
        public void onClick(View v) {
            int posistion = getAdapterPosition();
            TravelDeal selectedDeal = deals.get(posistion);
            Intent intent = new Intent(v.getContext(), DealActivity.class);
            intent.putExtra("Deal", selectedDeal);
            v.getContext().startActivity(intent);
        }
    }

    public void showImage(String url) {
        if (url != null && !url.isEmpty()) {
            Picasso.get()
                    .load(url)
                    .resize(250, 250)
                    .centerCrop()
                    .into(mDealImage);
        }
    }


}
