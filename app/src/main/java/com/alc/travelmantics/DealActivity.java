package com.alc.travelmantics;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

public class DealActivity extends AppCompatActivity {

    private static final int PICTURE_RESULT = 42;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;
    private EditText mTravelTitleTxt;
    private EditText mTravelPriceTxt;
    private EditText mDescriptionTxt;
    private TravelDeal mDeal;
    private ImageView mDealImage;

    private TextView mTravelTitleTv;
    private TextView mTravelPriceTv;
    private TextView mDescriptionTv;

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (FirebaseUtil.isAdmin) {
            setContentView(R.layout.activity_deal);
        } else {
            setContentView(R.layout.su_deal);
        }


        mFirebaseDatabase = FirebaseUtil.sFirebaseDatabase;
        mDatabaseReference = FirebaseUtil.sDatabaseReference;

        if (FirebaseUtil.isAdmin) {
            generateLayoutForAdmin();
        } else {
            generateLayoutForSimpleUser();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICTURE_RESULT && resultCode == RESULT_OK) {
            Uri imageUri = data.getData();
            final StorageReference ref = FirebaseUtil.sStorageReference.child(imageUri.getLastPathSegment());
            ref.putFile(imageUri).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        String imageUrl = task.getResult().toString();
                        String pictureName = task.getResult().getLastPathSegment();
                        mDeal.setImageUrl(imageUrl);
                        mDeal.setPictureName(pictureName);
                        showImage(imageUrl);
                    }
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_menu:
                saveDeal();
                Toast.makeText(this, "Dea done", Toast.LENGTH_LONG).show();
                clear();
                backToActivity();
                return true;
            case R.id.remove_menu:
                deleteDeal();
                Toast.makeText(this, "Dea deleted", Toast.LENGTH_LONG).show();
                backToActivity();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }

    }

    /**
     * ********* SAVE NEW DEAL **************
     */
    private void saveDeal() {
        mDeal.setTravelTitle(mTravelTitleTxt.getText().toString());
        mDeal.setDescription(mDescriptionTxt.getText().toString());
        mDeal.setTravelPrice(mTravelPriceTxt.getText().toString());

        if (mDeal.getId() == null) {
            mDatabaseReference.push().setValue(mDeal);
        } else {
            mDatabaseReference.child(mDeal.getId()).setValue(mDeal);
        }

    }

    /**
     * ********** DELETE DEAL ***********************
     */
    private void deleteDeal() {
        if (mDeal == null) {
            Toast.makeText(this, "Please save travel deal", Toast.LENGTH_LONG).show();
            return;
        }
        mDatabaseReference.child(mDeal.getId()).removeValue();
        if (mDeal.getPictureName() != null && !mDeal.getPictureName().isEmpty()) {
            StorageReference pictureRef = FirebaseUtil.sStorage.getReference().child(mDeal.getPictureName());
            pictureRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(DealActivity.this, "Deal deleted", Toast.LENGTH_LONG);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(DealActivity.this, "Error!! deal not deleted", Toast.LENGTH_LONG);
                }
            });
        }
    }

    private void backToActivity() {
        Intent intent = new Intent(this, ListeActivity.class);
        startActivity(intent);
    }

    private void clear() {
        mTravelTitleTxt.setText("");
        mDescriptionTxt.setText("");
        mTravelPriceTxt.setText("");
        mTravelTitleTxt.requestFocus();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_menu, menu);

        if (FirebaseUtil.isAdmin) {
            menu.findItem(R.id.remove_menu).setVisible(true);
            menu.findItem(R.id.save_menu).setVisible(true);
            // enableDeitText(true);


        } else {
            menu.findItem(R.id.remove_menu).setVisible(false);
            menu.findItem(R.id.save_menu).setVisible(false);
            //enableDeitText(false);
        }

        return true;
    }

    public void enableDeitText(boolean isEnabled) {
        mTravelTitleTxt.setEnabled(isEnabled);
        mTravelPriceTxt.setEnabled(isEnabled);
        mDescriptionTxt.setEnabled(isEnabled);
        findViewById(R.id.btn_upload).setEnabled(isEnabled);

    }

    public void showImage(String url) {
        if (url != null && !url.isEmpty()) {
            int width = Resources.getSystem().getDisplayMetrics().widthPixels;
            Picasso.get()
                    .load(url)
                    .resize(width, width * 2 / 3)
                    .centerCrop()
                    .into(mDealImage);
        }
    }

    private void generateLayoutForAdmin() {

        mTravelTitleTxt = (EditText) findViewById(R.id.txt_travel_title_su);
        mTravelPriceTxt = (EditText) findViewById(R.id.txt_travel_price_su);
        mDescriptionTxt = (EditText) findViewById(R.id.txt_description_su);

        mDealImage = (ImageView) findViewById(R.id.deal_image);

        Intent intent = getIntent();
        TravelDeal deal = (TravelDeal) intent.getSerializableExtra("Deal");

        if (deal == null) {
            deal = new TravelDeal();
        }
        this.mDeal = deal;


        mTravelTitleTxt.setText(mDeal.getTravelTitle());
        mTravelPriceTxt.setText(mDeal.getTravelPrice());
        mDescriptionTxt.setText(mDeal.getDescription());

        showImage(mDeal.getImageUrl());

        Button uploadButton = findViewById(R.id.btn_upload);
        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/jpeg");
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(intent.createChooser(intent, "Insert pictures"), PICTURE_RESULT);
            }
        });

    }

    private void generateLayoutForSimpleUser() {
        mTravelTitleTv = (TextView) findViewById(R.id.txt_travel_title_su);
        mTravelPriceTv = (TextView) findViewById(R.id.txt_travel_price_su);
        mDescriptionTv = (TextView) findViewById(R.id.txt_description_su);
        mDealImage = (ImageView) findViewById(R.id.deal_image_su);

        Intent intent = getIntent();
        TravelDeal deal = (TravelDeal) intent.getSerializableExtra("Deal");

        if (deal == null) {
            deal = new TravelDeal();
        }
        this.mDeal = deal;

        mTravelTitleTv.setText(mDeal.getTravelTitle());
        mTravelPriceTv.setText(mDeal.getTravelPrice());
        mDescriptionTv.setText(mDeal.getDescription());

        showImage(mDeal.getImageUrl());

    }
}

